package ssm.view;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import static java.nio.file.Files.deleteIfExists;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import static ssm.StartupConstants.PATH_BASE;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.file.SlideShowFileManager.JSON_EXT;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * This class provides the UI for the slide show viewer, note that this class is
 * a window and contains all controls inside.
 *
 * @author McKilla Gorilla & Rezaul Hassan 108822849
 */
public class SlideShowViewer extends Stage {

    //Directory builders
    boolean siteDir;
    boolean htmlDir;
    boolean cssDir;
    boolean jsDir;
    boolean imgDir;
    
    // THE MAIN UI
    SlideShowMakerView parentView;

    // THE DATA FOR THIS SLIDE SHOW
    SlideShowModel slides;
    
//    // HERE ARE OUR UI CONTROLS
//    BorderPane borderPane;
//    FlowPane topPane;
//    Label slideShowTitleLabel;
//    ImageView slideShowImageView;
//    VBox bottomPane;
//    Label captionLabel;
//    FlowPane navigationPane;
//    Button previousButton;
//    Button nextButton;

    /**
     * This constructor just initializes the parent and slides references, note
     * that it does not arrange the UI or start the slide show view window.
     *
     * @param initParentView Reference to the main UI.
     */
    public SlideShowViewer(SlideShowMakerView initParentView) {
	// KEEP THIS FOR LATER
	parentView = initParentView;

	// GET THE SLIDES
	slides = parentView.getSlideShow();
        String title = slides.getTitle();
        //build all the directories
        siteDir = new File("./sites").mkdirs();

        htmlDir = new File("./sites/" + title).mkdirs();
        cssDir = new File("./sites/" + title + SLASH + "css").mkdirs();
        jsDir = new File("./sites/" + title + SLASH + "js").mkdirs();
        //this checks if the image folder exists and clears it out beforehand.
        File chkImgPath = new File("./sites/" + title + SLASH + "img");
        for(File file: chkImgPath.listFiles()) file.delete();
        imgDir = new File("./sites/" + title + SLASH + "img").mkdirs();
        
    }

    /**
     * This method initializes the UI controls and opens the window with the
     * first slide in the slideshow displayed.
     */
    
    public void buildDir(){
        String title = slides.getTitle();
        
        //copy base html file to html directory
        Path source = Paths.get(PATH_BASE + "index.html");
        Path dest = Paths.get("./sites/" + title + SLASH + "index.html");
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
        //copy base css file to css directory
        source = Paths.get(PATH_BASE + "css//slideStyle.css");
        dest = Paths.get("./sites/" + title + SLASH + "css//slideStyle.css");
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
        //copy JSON slideshow into js directory
        source = Paths.get(PATH_SLIDE_SHOWS + SLASH + title + JSON_EXT);
        dest = Paths.get("./sites/" + title + SLASH + "js/" + title + JSON_EXT);
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
        //copy base js file to js directory
        source = Paths.get(PATH_BASE + "js//magic.js");
        dest = Paths.get("./sites/" + title + SLASH + "js//magic.js");
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
/////////////////////////////////////
        //copy base js file to js directory
        source = Paths.get(PATH_BASE + "js//cookie.js");
        dest = Paths.get("./sites/" + title + SLASH + "js//cookie.js");
        try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\        

        //get source of each slide image and copy them to img directory
        ObservableList<Slide> slideList = slides.getSlides();
	for (Slide slide : slideList) {
            source = Paths.get(slide.getImagePath() + slide.getImageFileName());
            dest = Paths.get("./sites/" + title + SLASH + "img//" + slide.getImageFileName());
            try {
            Files.copy(source, dest, StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
	}       
    }
    
    public void startSlideShow(){
        String title = slides.getTitle();
        File f = new File("./sites/" + title + SLASH + "index.html");
 
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();

        try {
            webEngine.load(f.toURI().toURL().toString());
        } catch (MalformedURLException ex) {
            Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
        }

        Scene scene = new Scene(webView, 660, 700);
        setScene(scene);
	this.showAndWait();
    }
    
//    public void startSlideShow() {
//	// FIRST THE TOP PANE
//	topPane = new FlowPane();
//	topPane.setAlignment(Pos.CENTER);
//	slideShowTitleLabel = new Label(slides.getTitle());
//	slideShowTitleLabel.getStyleClass().add(LABEL_SLIDE_SHOW_TITLE);
//	topPane.getChildren().add(slideShowTitleLabel);
//
//	// THEN THE CENTER, START WITH THE FIRST IMAGE
//	slideShowImageView = new ImageView();
//	reloadSlideShowImageView();
//
//	// THEN THE BOTTOM PANE
//	bottomPane = new VBox();
//	bottomPane.setAlignment(Pos.CENTER);
//	captionLabel = new Label();
//	if (slides.getSlides().size() > 0) {
//	    captionLabel.setText(slides.getSelectedSlide().getCaption());
//	}
//	navigationPane = new FlowPane();
//	bottomPane.getChildren().add(captionLabel);
//	bottomPane.getChildren().add(navigationPane);
//
//	// NOW SETUP THE CONTENTS OF THE NAVIGATION PANE
//	navigationPane.setAlignment(Pos.CENTER);
//	previousButton = parentView.initChildButton(navigationPane, ICON_PREVIOUS, LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
//	nextButton = parentView.initChildButton(navigationPane, ICON_NEXT, LanguagePropertyType.TOOLTIP_NEXT_SLIDE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
//
//	// NOW ARRANGE ALL OUR REGIONS
//	borderPane = new BorderPane();
//	borderPane.setTop(topPane);
//	borderPane.setCenter(slideShowImageView);
//	borderPane.setBottom(bottomPane);
//
//	// NOW SETUP THE BUTTON HANDLERS
//	previousButton.setOnAction(e -> {
//	    slides.previous();
//	    reloadSlideShowImageView();
//	    reloadCaption();
//	});
//	nextButton.setOnAction(e -> {
//	    slides.next();
//	    reloadSlideShowImageView();
//	    reloadCaption();
//	});
//
//	// NOW PUT STUFF IN THE STAGE'S SCENE
//	Scene scene = new Scene(borderPane, 1000, 700);
//	setScene(scene);
//	this.showAndWait();
//    }
//
//    // HELPER METHOD
//    private void reloadSlideShowImageView() {
//	try {
//	    Slide slide = slides.getSelectedSlide();
//	    if (slide == null) {
//		slides.setSelectedSlide(slides.getSlides().get(0));
//	    }
//	    slide = slides.getSelectedSlide();
//	    String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
//	    File file = new File(imagePath);
//	    
//	    // GET AND SET THE IMAGE
//	    URL fileURL = file.toURI().toURL();
//	    Image slideImage = new Image(fileURL.toExternalForm());
//	    slideShowImageView.setImage(slideImage);
//
//	    // AND RESIZE IT
//	    double scaledHeight = DEFAULT_SLIDE_SHOW_HEIGHT;
//	    double perc = scaledHeight / slideImage.getHeight();
//	    double scaledWidth = slideImage.getWidth() * perc;
//	    slideShowImageView.setFitWidth(scaledWidth);
//	    slideShowImageView.setFitHeight(scaledHeight);
//	} catch (Exception e) {
//	    // CANNOT SHOW A SLIDE SHOW WITHOUT ANY IMAGES
//	    parentView.getErrorHandler().processError(LanguagePropertyType.ERROR_NO_SLIDESHOW_IMAGES);
//	}
//    }
//
//    private void reloadCaption() {
//	Slide slide = slides.getSelectedSlide();
//	captionLabel.setText(slide.getCaption());
//    }
}
