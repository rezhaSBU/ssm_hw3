//This file was an attempt to build the slideshow within a canvas. It is solely for testing and not actually used nor referenced in the project.

var jsonFile;
var ready = 0;
var imgArray = [];
var srcArray = [];
var capArray = [];

function writeHere(){
//get the image_file_names and captions for each slide and put in respective arrays
jsonFile = document.location.toString().split('/');

$.getJSON( "./js/" + jsonFile[jsonFile.length-2] + ".json", function(data){
    $.each(data.slides, function(index, slides){
        var temp = document.createElement('img');
        imgArray.push(temp);
        srcArray.push("./img/" + slides.image_file_name);
        capArray.push(slides.caption);
    });
//set the header of the page as Slideshow's title
document.getElementById('title').innerHTML = decodeURIComponent(jsonFile[jsonFile.length-2]);

//checks to see all images are loaded
for (i = 0; i < imgArray.length; i++){
      imgArray[i].onload = function(e) {
            ready++;
            if (ready === imgArray.length) {
	//build a canvas and display the images in it
	var counter = 0, delayinms = 4000, maxNum = imgArray.length-1,

     	canvas = document.getElementById('slideShow'),
     	context = canvas.getContext('2d'),
	me = this;

	this._draw = function(){
		context.clearRect(0,0, canvas.width, canvas.height);
if( (canvas.width / imgArray[counter].width) > (canvas.height / imgArray[counter].height) ){
		context.drawImage(imgArray[counter], 0, 0, imgArray[counter].width * canvas.height / imgArray[counter].height, imgArray[counter].height * canvas.height / imgArray[counter].height);
}
else{		context.drawImage(imgArray[counter], 0, 0, imgArray[counter].width * canvas.width / imgArray[counter].width, imgArray[counter].height * canvas.width / imgArray[counter].width);
}
		counter++;
		if (counter > maxNum) counter = 0;
		setTimeout(me._draw, 3000);
	}
     	
	this._draw();
            }
      }
}

//loads images
for (i = 0; i < imgArray.length; i++){
      imgArray[i].src = srcArray[i];
}

});
}