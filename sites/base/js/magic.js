//This javascript reads in the title of the slideshow and the images and caption of each slide to the project from a JSON file in the same directory. 

var jsonFile;
var items = 0;
var count = 0;
var timer;
var imgArray = [];
var capArray = [];
var alevia;

function writeHere(){

jsonFile = document.location.toString().split('/');

//set the header of the page as Slideshow's title
document.getElementById('title').innerHTML = decodeURIComponent(jsonFile[jsonFile.length-2]);

$.getJSON( "./js/" + jsonFile[jsonFile.length-2] + ".json", function(data){
	$.each(data.slides, function(index, slides){
		imgArray.push(slides.image_file_name);
		capArray.push(slides.caption);
	});
    var x = document.createElement("IMG");
    x.setAttribute("id", "alevia");
    x.setAttribute("src", "./img/" + imgArray[0]);
    x.setAttribute("align", "center");
    x.setAttribute("alt", capArray[0]);
    document.getElementById("slideshow").appendChild(x);
    document.getElementById("caption").innerHTML = (capArray[0]);

    timer = setInterval("nextImage()", 2200);
});
}


function nextImage() {
items = items + 1;

if(items < 0) items = imgArray.length-1;
if(items > imgArray.length-1) items = 0;
    var x = document.createElement("IMG");
    x.setAttribute("id", "alevia");
    x.setAttribute("src", "./img/" + imgArray[items]);
    x.setAttribute("align", "center");
    x.setAttribute("alt", capArray[items]);
    document.getElementById("slideshow").appendChild(x);
    document.getElementById("caption").innerHTML = (capArray[items]);

alevia = document.getElementById("alevia");
alevia.parentNode.removeChild(x.previousSibling);

count = count +1;
}


function prevImage() {
items = items - 1;

if(items < 0) items = imgArray.length-1;
if(items > imgArray.length-1) items=0;
    var x = document.createElement("IMG");
    x.setAttribute("id", "alevia");
    x.setAttribute("src", "./img/" + imgArray[items]);
    x.setAttribute("align", "center");
    x.setAttribute("alt", capArray[items]);
    document.getElementById("slideshow").appendChild(x);
    document.getElementById("caption").innerHTML = (capArray[items]);

alevia = document.getElementById("alevia");
alevia.parentNode.removeChild(x.previousSibling);

count = count +1;
}

function play(){
timer = setInterval("nextImage()", 2200);
$('#pause').show();
$('#play').hide();
}

function pause(){
clearInterval(timer);
$('#pause').hide();
$('#play').show();
}